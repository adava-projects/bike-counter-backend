import datetime
import os
import calendar
import flask
import flask_sqlalchemy
import flask_praetorian
import flask_cors
from flask import jsonify
from sqlalchemy import extract

db = flask_sqlalchemy.SQLAlchemy()
guard = flask_praetorian.Praetorian()
cors = flask_cors.CORS()


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.Text, unique=True)
    password = db.Column(db.Text)
    roles = db.Column(db.Text)
    is_active = db.Column(db.Boolean, default=True, server_default='true')

    @property
    def rolenames(self):
        try:
            return self.roles.split(',')
        except Exception:
            return []

    @classmethod
    def lookup(cls, username):
        return cls.query.filter_by(username=username).one_or_none()

    @classmethod
    def identify(cls, id):
        return cls.query.get(id)

    @property
    def identity(self):
        return self.id

    def is_valid(self):
        return self.is_active


class BikeData(db.Model):
    """
    This table contains bike count data,
    There must be only one row per location and per month
    """
    id = db.Column(db.Integer, primary_key=True)

    date = db.Column(db.Date, nullable=False)
    begin_time = db.Column(db.Time, nullable=True)
    end_time = db.Column(db.Time, nullable=True)

    location = db.Column(db.String(100), nullable=False)

    meta_data = db.Column(db.JSON, nullable=True)

    data = db.Column(db.JSON, nullable=False)

    @classmethod
    def get_data(cls, location: str, date: datetime.date):
        data = db.session.query(cls).filter_by(location=location).filter(
            extract('year', cls.date) == date.year).filter(extract('month', cls.date) == date.month).all()

        return data

    @classmethod
    def insert(cls, object: "BikeData"):

        date = object.date
        location = object.location

        existing_data = cls.get_data(location=location, date=date)

        if existing_data:
            raise Exception(f"Cannot insert data {object}. Bike data with same "
                            f"month and location exists.")
        else:
            db.session.add(object)


# A generic user model that might be used by an app powered by flask-praetorian


# Initialize flask app for the example
app = flask.Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = os.getenv("SECRET_KEY")
app.config['JWT_ACCESS_LIFESPAN'] = {'hours': 24}
app.config['JWT_REFRESH_LIFESPAN'] = {'days': 30}

# Initialize the flask-praetorian instance for the app
guard.init_app(app, User)

# Initialize a local database for the example
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("DATABASE_URI")
db.init_app(app)

# Initializes CORS so that the api_tool can talk to the example app
cors.init_app(app)

# Add users for the example
with app.app_context():
    try:
        db.create_all()
        if db.session.query(User).filter_by(username='Yasoob').count() < 1:
            db.session.add(User(
                username='Yasoob',
                password=guard.hash_password('strongpassword'),
                roles='admin'
            ))
        db.session.commit()
    except Exception:
        pass


# Set up some routes for the example
@app.route('/api/')
def home():
    return {"Hello": "World"}, 200


@app.route('/api/submit_bike_data', methods=['POST'])
# @flask_praetorian.auth_required
def submit_bike_data():
    json_data = flask.request.get_json(force=True)

    json_data["date"] = datetime.date.fromisoformat(json_data["date"])
    json_data["begin_time"] = datetime.time.fromisoformat(json_data["begin_time"])

    if json_data["end_time"]:
        json_data["end_time"] = datetime.time.fromisoformat(json_data["end_time"])
    else:
        json_data["end_time"] = None

    db.session.query(BikeData)

    bike_data = BikeData(**json_data)

    data = BikeData.get_data(bike_data.location, bike_data.date)
    if data:
        db.session.delete(data[0])

    BikeData.insert(bike_data)

    db.session.commit()

    return {"message": "ok"}, 200


@app.route('/api/retrieve_bike_data')
def retreive_bike_data():
    date = datetime.date.fromisoformat(flask.request.args.get("date"))
    location = flask.request.args.get("location")

    data = BikeData.get_data(location, date)

    if not data:
        return "No row found for given date and location. Error.", 400

    data = data[0]

    return_value = {
        "date": data.date,
        "begin_time": data.begin_time,
        "end_time": data.end_time,
        "location": data.location,
        "data": data.data,
        "meta_data": data.meta_data
    }

    # FIXME fix utf8, ex : Avenue Saint J\u00e9r\u00f4me
    return return_value, 200


@app.route('/api/login', methods=['POST'])
def login():
    """
    Logs a user in by parsing a POST request containing user credentials and
    issuing a JWT token.
    .. example::
       $ curl http://localhost:5000/api/login -X POST \
         -d '{"username":"Yasoob","password":"strongpassword"}'
    """
    req = flask.request.get_json(force=True)
    username = req.get('username', None)
    password = req.get('password', None)
    user = guard.authenticate(username, password)
    ret = {'access_token': guard.encode_jwt_token(user)}
    return ret, 200


@app.route('/api/refresh', methods=['POST'])
def refresh():
    """
    Refreshes an existing JWT by creating a new one that is a copy of the old
    except that it has a refrehsed access expiration.
    .. example::
       $ curl http://localhost:5000/api/refresh -X GET \
         -H "Authorization: Bearer <your_token>"
    """
    print("refresh request")
    old_token = flask.request.get_data()
    new_token = guard.refresh_jwt_token(old_token)
    ret = {'access_token': new_token}
    return ret, 200


@app.route('/api/protected')
@flask_praetorian.auth_required
def protected():
    """
    A protected endpoint. The auth_required decorator will require a header
    containing a valid JWT
    .. example::
       $ curl http://localhost:5000/api/protected -X GET \
         -H "Authorization: Bearer <your_token>"
    """
    return {'message': f'protected endpoint (allowed user {flask_praetorian.current_user().username})'}


@app.route('/api/graphs')
# @flask_praetorian.auth_required
def graphs():
    """
    Returns a list of graphs data.
    Each graph data is a dict with 2 keys : data and layout.
    To be used in a Plotly.plot object
    """

    from make_plots import make_map, make_plots

    return jsonify({"graphs_data": make_plots(), "map_data": make_map()}), 200


# calculate time series msg perc croissance
@app.route('/api/msg_perc_croissance', methods=['POST'])
def msg_perc_croissance():
    from make_plots import msg_perc_croissance

    req = flask.request.get_json(force=True)
    time_serie = req.get('time_serie', None)
    index_prem_mois = req.get('index_prem_mois', None)
    index_der_mois = req.get('index_der_mois', None)
    tendance_prefix = req.get('tendance_prefix', "Tendance")

    if None in [time_serie, index_prem_mois, index_der_mois]:
        return "Missing parameters", 400

    import pandas as pd
    time_serie = {pd.to_datetime(k): v for k, v in time_serie.items()}
    time_serie = pd.DataFrame(index=time_serie.keys(), data=time_serie.values())

    return jsonify({"msg": msg_perc_croissance(time_serie, index_prem_mois, index_der_mois, tendance_prefix)}), 200


@app.route('/test_plots')
def test_plots():
    """
    Returns template test.html
    """
    return flask.render_template('test.html')


if __name__ == '__main__':
    app.run(host="0.0.0.0")
