import csv
import datetime

from sqlalchemy.orm.attributes import flag_modified

from app import BikeData, app, db


def run():

    with open("data_adava_prep_octobre_21.csv", newline="") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=";", quotechar="|")

        for index, row in enumerate(reader):
            print(f"processing row {index}")
            py_datetime = datetime.date.fromisoformat(row["date"].split(" ")[0])
            bike_data = BikeData.query.filter_by(location=row["lieu"], date=py_datetime).one_or_none()
            if bike_data is None:
                bike_data = BikeData(location=row["lieu"],
                                                    date=py_datetime,
                                                    meta_data={},
                                                    data={})

                db.session.add(bike_data)

            dir_voie = row["dir_voie"]
            dir_voie_key = dir_voie.split(":")[0].rstrip()

            if dir_voie_key not in bike_data.data:
                bike_data.data[dir_voie_key] = {}

            if "trottoir" in dir_voie:
                type_voie = "trottoir"
            else:
                type_voie = "chaussee"

            if type_voie not in bike_data.data[dir_voie_key]:
                bike_data.data[dir_voie_key][type_voie] = {}

            bike_data.data[dir_voie_key][type_voie].update({row["type_velo"]: int(row["nombre"])})

            flag_modified(bike_data, "data")
    db.session.commit()

            # if index > 300:
            #     break

    # flat_data = []
    #
    # db_bike_objs = BikeData.query.all()
    #
    # for db_bike_obj in db_bike_objs:
    #     for dir_voie_key, dir_voie_value in db_bike_obj.data.items():
    #         for type_voie_key, type_voie_data in dir_voie_value.items():
    #             for type_velo, nb_velo in type_voie_data.items():
    #                 flat_data.append(
    #                     {
    #                         "lieu": db_bike_obj.location,
    #                         "dir_voie": dir_voie_key,
    #                         "type_voie": type_voie_key,
    #                         "type_velo": type_velo,
    #                         "date": db_bike_obj.date,
    #                         "nombre": nb_velo
    #                     }
    #                 )
    #
    # for flat in flat_data:
    #     print(flat)



if __name__ == '__main__':
    with app.app_context():
        run()
