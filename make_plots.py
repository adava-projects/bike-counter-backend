# -*- coding: utf-8 -*-
"""
Created on Sun Oct 17 12:18:34 2021

@author: Elise
"""

import locale

from app import BikeData

locale.setlocale(locale.LC_TIME, "")

import matplotlib.pyplot as plt
import seaborn as sns

sns.set(style="whitegrid")
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()

# pour afficher les plots (fig.show()) dans son navigateur :
# import plotly.io as pio
# pio.renderers.default='browser'


import pandas as pd
import numpy as np

from datetime import datetime
from dateutil.relativedelta import relativedelta
from statsmodels.tsa.seasonal import seasonal_decompose

import geopandas as gpd
from shapely.geometry import Point

import plotly.express as px


# paramètres de taille du texte sur les graphiques
plt.rc("font", size=18)
plt.rc("axes", titlesize=20)
plt.rc("axes", labelsize=18)
plt.rc("xtick", labelsize=16)
plt.rc("ytick", labelsize=16)
plt.rc("legend", fontsize=14)
plt.rc("figure", titlesize=22)


def get_bike_data() -> list:

    flat_data = []

    db_bike_objs = BikeData.query.all()

    for db_bike_obj in db_bike_objs:
        for dir_voie_key, dir_voie_value in db_bike_obj.data.items():
            for type_voie_key, type_voie_data in dir_voie_value.items():
                for type_velo, nb_velo in type_voie_data.items():
                    flat_data.append(
                        {
                            "lieu": db_bike_obj.location,
                            "dir_voie": dir_voie_key,
                            "type_voie": type_voie_key,
                            "type_velo": type_velo,
                            "date": pd.to_datetime(db_bike_obj.date),
                            "nombre": int(nb_velo),
                        }
                    )

    return flat_data


# -Tendance de l'évolution des comptages : fonction pour l'afficher en texte sur le graph
def msg_perc_croissance(time_serie, index_prem_mois, index_der_mois, message_prefix="Tendance"):

    # 1er et dernier mois pour les encarts avec le % de croissance 'sept.2016', 'janv.2022'
    prem_mois = time_serie.index[index_prem_mois]
    prem_mois = prem_mois.strftime("%b%Y")

    der_mois = time_serie.index[index_der_mois]
    der_mois = der_mois.strftime("%b%Y")

    # calcul de la tendance
    time_serie_decompose = seasonal_decompose(
        time_serie, model="additive", period=10, extrapolate_trend=1
    )

    # croissance de la tendance
    trend1 = time_serie_decompose.trend.iloc[index_prem_mois]
    trend_der = time_serie_decompose.trend.iloc[index_der_mois]
    perc_croissance = int(round((trend_der - trend1) / trend1 * 100))

    if perc_croissance > 0:
        text_perc_croissance = "+" + str(perc_croissance)
    else:
        text_perc_croissance = str(perc_croissance)

    return message_prefix+" : " + text_perc_croissance + "% de " + prem_mois + " à " + der_mois


def make_plots():

    graphs_data_list = []

    flat_data = get_bike_data()

    data_adava = pd.DataFrame(flat_data)

    # convertir les dates en format date
    data_adava["date"] = pd.to_datetime(data_adava["date"])

    # enlever les données de juillet et août(peu nombreuses)
    juil_aout = (pd.DatetimeIndex(data_adava["date"]).month == 7) | (
        pd.DatetimeIndex(data_adava["date"]).month == 8
    )
    data_adava = data_adava[~juil_aout]

    # enlever les données de mai et juin 2016 où il y a pas mal de points de comptages manquants
    year_month = pd.to_datetime(data_adava["date"]).dt.to_period("M")
    mai_juin_16 = (year_month == "2016-05") | (year_month == "2016-06")
    data_adava = data_adava[~mai_juin_16]

    data_adava.reset_index(drop=True, inplace=True)

    # erreurs sur les dates : mettre toutes les dates avec day=1 et h:m:s.ms = 00:00:00.000
    data_adava["date"] = data_adava["date"].apply(
        lambda t: datetime(int(t.year), int(t.month), 1, 0, 0, 0, 0)
    )

    # ------GRAPHS COMPTAGES

    der_mois = "jan.22"

    # ---Nombre total de vélos recensés

    data_by_month = data_adava.groupby(["date"], as_index=False).sum()
    ts_by_month = data_by_month.set_index("date")

    # -Décomposition de la série temporelle

    # on enlève mars et avril 2020 pour ne pas biaiser la tendance
    # ts_by_month = ts_by_month[~ts_by_month.index.isin([datetime(2020,3,1),datetime(2020,4,1)])]
    # v2 : plutôt remplacer par la valeur de février 2020 pour que ça ne gène pas la fréquence de 10
    ts_by_month.loc[
        ts_by_month.index.isin([datetime(2020, 3, 1), datetime(2020, 4, 1)]), "nombre"
    ] = ts_by_month.loc[ts_by_month.index.isin([datetime(2020, 2, 1)])].nombre[0]

    ts_by_month_decompose = seasonal_decompose(
        ts_by_month, model="additive", period=10, extrapolate_trend=1
    )

    # -Tendance de l'évolution des comptages

    # extraire les 1ère et dernière valeurs de la trend pour les afficher sur le graph
    trend1 = ts_by_month_decompose.trend.iloc[0]  # trend (ordonnée)
    trend1_mois = ts_by_month_decompose.trend.index[0]  # mois (abscisse)
    trend_der = ts_by_month_decompose.trend.iloc[-1]
    trend_der_mois = ts_by_month_decompose.trend.index[-1]

    # croissance de la tendance
    perc_croissance = str(int(round((trend_der - trend1) / trend1 * 100)))

    # -Graph
    fig = px.line(
        ts_by_month,
        x=ts_by_month.index,
        y="nombre",
        labels={"date": "Mois de comptage", "nombre": "Nombre de vélos"},
        title="Comptages de vélos - Tendance de l'évolution",
    )

    fig.add_scatter(
        x=ts_by_month_decompose.trend.index,
        y=round(ts_by_month_decompose.trend),
        mode="lines",
        name="Tendance : moyenne glissante sur un an",
    )

    fig.update_layout(
        showlegend=True,
        legend=dict(yanchor="top", y=0.99, xanchor="left", x=0.01),
        font=dict(size=18),
    )

    fig.add_annotation(
        x=1,
        y=0.8,
        text=msg_perc_croissance(ts_by_month, 0, -1),
        showarrow=False,
        xref="paper",
        xanchor="right",
        yref="paper",
        yanchor="top",
        font=dict(color="#ffffff"),
        bgcolor="red",
        opacity=0.5,
    )

    fig.update_yaxes(range=[0, ts_by_month.nombre.max() * 1.1], autorange=False)
    # fig.show()
    graphs_data_list.append(fig.to_json())

    # ---Nombre moyen de vélos par comptage

    # série temporelle du nombre de vélo moyen (par lieu) compté par mois
    data_by_month_tot_velos = data_adava.groupby(["date", "lieu"], as_index=False).sum()
    data_by_month_mean = data_by_month_tot_velos.groupby(
        ["date"], as_index=False
    ).mean()
    ts_by_month_mean = data_by_month_mean.set_index("date")
    ts_by_month_mean = round(ts_by_month_mean)

    # décomposition de la série temporelle
    ts_by_month_mean_decompose = seasonal_decompose(
        ts_by_month_mean, model="additive", period=10, extrapolate_trend=1
    )

    # extraire les 1ère et dernière valeurs de la trend pour les afficher sur le graph
    trend1 = ts_by_month_mean_decompose.trend.iloc[0]
    trend_der = ts_by_month_mean_decompose.trend.iloc[-1]

    # croissance de la tendance
    perc_croissance = str(int(round((trend_der - trend1) / trend1 * 100)))

    # -Graph
    fig = px.line(
        ts_by_month_mean,
        x=ts_by_month_mean.index,
        y="nombre",
        labels={"date": "Mois de comptage", "nombre": "Nombre de vélos"},
        title="Nombre moyen de vélos enregistrés par lieu (en 1h)",
    )
    fig.add_scatter(
        x=ts_by_month_mean_decompose.trend.index,
        y=round(ts_by_month_mean_decompose.trend),
        mode="lines",
        name="Tendance : moyenne glissante sur un an",
    )

    fig.update_layout(
        showlegend=True,
        legend=dict(yanchor="top", y=0.99, xanchor="left", x=0.01),
        font=dict(size=18),
    )

    fig.add_annotation(
        x=1,
        y=0.8,
        text= msg_perc_croissance(ts_by_month_mean, 0, -1),
        xref="paper",
        xanchor="right",
        yref="paper",
        yanchor="top",
        showarrow=False,
        font=dict(color="#ffffff"),
        bgcolor="red",
        opacity=0.5,
    )
    fig.update_yaxes(range=[0, ts_by_month_mean.nombre.max() * 1.1], autorange=False)
    # fig.show()
    graphs_data_list.append(fig.to_json())

    # ---Nombre moyen de vélos par lieu par an

    data_an_mean = (
        ts_by_month_mean.groupby(ts_by_month_mean.index.year).sum().drop([2016])
    )

    croissance_1 = round(
        (data_an_mean.loc[2018] - data_an_mean.loc[2017])
        / data_an_mean.loc[2017]
        * 100,
        1,
    )
    croissance_2 = round(
        (data_an_mean.loc[2019] - data_an_mean.loc[2018])
        / data_an_mean.loc[2018]
        * 100,
        1,
    )
    croissance_3 = round(
        (data_an_mean.loc[2021] - data_an_mean.loc[2019])
        / data_an_mean.loc[2019]
        * 100,
        1,
    )

    # -Graph
    fig = px.bar(
        round(data_an_mean),
        x=data_an_mean.index,
        y="nombre",
        title="Nombre de vélos comptés par lieu en moyenne (hors juillet-août)",
        labels={"nombre": "Nombre de vélos", "date": "Année"},
    )
    fig.update_traces(marker_color="gold", width=0.3)
    fig.add_annotation(
        x=2018,
        y=200,
        text="+" + str(croissance_1[0]) + "% <br>(17 à 18)",
        showarrow=False,
        xanchor="right",
        font=dict(color="#ffffff"),
        bgcolor="red",
        opacity=0.8,
    )
    fig.add_annotation(
        x=2019,
        y=200,
        text="+" + str(croissance_2[0]) + "% <br>(18 à 19)",
        showarrow=False,
        xanchor="right",
        font=dict(color="#ffffff"),
        bgcolor="red",
        opacity=0.8,
    )
    fig.add_annotation(
        x=2021,
        y=200,
        text="+" + str(croissance_3[0]) + "% <br>(19 à 21)",
        showarrow=False,
        xanchor="right",
        font=dict(color="#ffffff"),
        bgcolor="red",
        opacity=0.8,
    )
    # fig.show()
    graphs_data_list.append(fig.to_json())

    # ---Saisonnalité

    ts_saisonnalite = (
        ts_by_month_mean_decompose.seasonal + ts_by_month_mean.mean()["nombre"]
    )

    saisonnalite = round(ts_by_month_mean_decompose.seasonal.iloc[4:14], 1)
    saisonnalite.index = [
        "Janvier",
        "Février",
        "Mars",
        "Avril",
        "Mai",
        "Juin",
        "Septembre",
        "Octobre",
        "Novembre",
        "Décembre",
    ]

    # en ajoutant le nombre de vélos moyens qui passent
    saisonnalite_val = saisonnalite + ts_by_month_mean.mean()["nombre"]

    # -Graph
    fig = px.bar(
        x=saisonnalite_val.index,
        y=round(saisonnalite_val),
        title="Nombre moyen de vélos enregistrés en 1h par lieu : impact de la saisonnalité",
        labels={"x": "Mois", "y": "Nombre moyen de vélos"},
    )
    fig.update_traces(marker_color="gold", width=0.3)
    fig.add_annotation(
        x=5.5,
        y=62,
        text="Pas de comptages <br>en juillet/août",
        showarrow=False,
        xanchor="right",
        font=dict(color="#ffffff"),
        bgcolor="orange",
        opacity=0.8,
    )
    fig.add_annotation(
        x=9,
        y=ts_by_month_mean.mean().nombre + 2,
        text="Moyenne",
        showarrow=False,
        xanchor="right",
        font=dict(color="blue"),
    )
    fig.add_shape(
        type="line",
        x0=-1,
        y0=ts_by_month_mean.mean().nombre,
        x1=10,
        y1=ts_by_month_mean.mean().nombre,
        line=dict(
            color="blue",
        ),
    )
    # fig.show()
    graphs_data_list.append(fig.to_json())

    # ---Nombre de vélos recensés par type de vélo

    # série temporelle du nombre de vélo moyen (par lieu et type de vélo) compté par mois
    data_by_month_tot_velos_type = data_adava.groupby(
        ["date", "lieu", "type_velo"], as_index=False
    ).sum()
    data_by_month_type_mean = data_by_month_tot_velos_type.groupby(
        ["date", "type_velo"], as_index=False
    ).mean()
    ts_by_month_type_mean = data_by_month_type_mean.set_index("date")

    # décomposition de la série temporelle
    ts_by_month_type_mean_p = ts_by_month_type_mean.pivot(
        columns="type_velo", values="nombre"
    )
    ts_by_month_type_mean_decompose_velo = seasonal_decompose(
        ts_by_month_type_mean_p["Vélos"],
        model="additive",
        period=10,
        extrapolate_trend=1,
    )
    ts_by_month_type_mean_decompose_vae = seasonal_decompose(
        ts_by_month_type_mean_p["VAE"], model="additive", period=10, extrapolate_trend=1
    )

    # extraire les 1ère et dernière valeurs de la trend
    trend1_vae = ts_by_month_type_mean_decompose_vae.trend[
        ~np.isnan(ts_by_month_type_mean_decompose_vae.trend)
    ].iloc[0]
    trend_der_vae = ts_by_month_type_mean_decompose_vae.trend[
        ~np.isnan(ts_by_month_type_mean_decompose_vae.trend)
    ].iloc[-1]
    trend1_velo = ts_by_month_type_mean_decompose_velo.trend[
        ~np.isnan(ts_by_month_type_mean_decompose_velo.trend)
    ].iloc[0]
    trend_der_velo = ts_by_month_type_mean_decompose_velo.trend[
        ~np.isnan(ts_by_month_type_mean_decompose_velo.trend)
    ].iloc[-1]
    trend_date_avder = ts_by_month_type_mean_decompose_velo.trend[
        ~np.isnan(ts_by_month_type_mean_decompose_velo.trend)
    ].index[-3]

    # croissance de la tendance
    perc_croissance_vae = str(
        int(round((trend_der_vae - trend1_vae) / trend1_vae * 100))
    )
    perc_croissance_velo = str(
        int(round((trend_der_velo - trend1_velo) / trend1_velo * 100))
    )

    # -Graph
    fig = px.line(
        round(ts_by_month_type_mean),
        x=ts_by_month_type_mean.index,
        y="nombre",
        color="type_velo",
        color_discrete_sequence=["skyblue", "darkseagreen"],
        line_dash_sequence=["dash", "dash"],
        labels={"date": "Mois de comptage", "nombre": "Nombre de vélos"},
        title="Nombre moyen de passages enregistrés en 1h par lieu selon le type de vélo",
    )
    fig.add_scatter(
        x=ts_by_month_type_mean_decompose_velo.trend.index,
        y=round(ts_by_month_type_mean_decompose_velo.trend),
        mode="lines",
        name="Tendance vélo",
        line=dict(color="green"),
    )
    fig.add_scatter(
        x=ts_by_month_type_mean_decompose_vae.trend.index,
        y=round(ts_by_month_type_mean_decompose_vae.trend),
        mode="lines",
        name="Tendance VAE",
        line=dict(color="dodgerblue"),
    )
    fig.update_layout(showlegend=False, font=dict(size=18))

    fig.add_annotation(
        x=1,
        y=0.3,
        text=msg_perc_croissance(ts_by_month_type_mean_p["VAE"], 0, -1, "Tendance VAE"),
        showarrow=False,
        xref="paper",
        xanchor="right",
        yref="paper",
        yanchor="top",
        font=dict(color="#ffffff"),
        bgcolor="dodgerblue",
        opacity=0.5,
    )
    fig.add_annotation(
        x=1,
        y=0.7,
        text=msg_perc_croissance(ts_by_month_type_mean_p["Vélos"], 0, -1, "Tendance vélo classique"),
        showarrow=False,
        xref="paper",
        xanchor="right",
        yref="paper",
        yanchor="top",
        font=dict(color="#ffffff"),
        bgcolor="green",
        opacity=0.5,
    )

    fig.add_annotation(
        x=1,
        y=1,
        text="Tendance : moyenne glissante sur un an",
        showarrow=False,
        xref="paper",
        xanchor="right",
        yref="paper",
        yanchor="top",
    )

    fig.update_yaxes(
        range=[0, ts_by_month_type_mean.nombre.max() * 1.1], autorange=False
    )
    # fig.show()
    graphs_data_list.append(fig.to_json())

    # ---Nombre de vélos recensés par lieu

    # série temporelle du nombre de vélo (par lieu) compté par mois
    data_by_month_lieu = data_adava.groupby(["date", "lieu"], as_index=False).sum()
    ts_by_month_lieu = data_by_month_lieu.set_index("date")
    ts_by_month_lieu_p = ts_by_month_lieu.pivot(columns="lieu", values="nombre")

    # imputation des données manquantes
    # choix : calculer la moyenne annuelle (+/- 6 mois) sur ce lieu et ajouter la composante saisonnière calculée plus haut (saisonnalité)
    ts_by_month_lieu_imp = ts_by_month_lieu_p
    for lieu1 in ts_by_month_lieu_p.columns:
        for date1 in ts_by_month_lieu_p.index:
            if np.isnan(ts_by_month_lieu_p.loc[date1, lieu1]):
                moy_an_lieu = ts_by_month_lieu_p.loc[
                    (date1 - relativedelta(months=7)) : (
                        date1 + relativedelta(months=7)
                    ),
                    lieu1,
                ].mean()
                sais_mois = ts_by_month_mean_decompose.seasonal.loc[date1]
                ts_by_month_lieu_imp.loc[date1, lieu1] = round(
                    moy_an_lieu + sais_mois, 3
                )  # .nombre

    # décomposition de la série temporelle
    ts_by_month_lieu_decompose_trend = pd.DataFrame(columns=ts_by_month_lieu_p.columns)
    ts_by_month_lieu_decompose_seasonal = pd.DataFrame(
        columns=ts_by_month_lieu_p.columns
    )

    for lieu1 in ts_by_month_lieu_p.columns:
        ts_by_month_lieu_decompose_trend[lieu1] = seasonal_decompose(
            ts_by_month_lieu_imp[lieu1],
            model="additive",
            period=10,
            extrapolate_trend=1,
        ).trend
        ts_by_month_lieu_decompose_seasonal[lieu1] = seasonal_decompose(
            ts_by_month_lieu_imp[lieu1],
            model="additive",
            period=10,
            extrapolate_trend=1,
        ).seasonal

    liste_lieux = ts_by_month_lieu_decompose_trend.columns

    # -dates des travaux
    dates_travaux = pd.DataFrame(index=liste_lieux, columns=["date_debut", "date_fin"])

    dates_travaux.loc[
        dates_travaux.index == "Avenue de l'Europe", "date_debut"
    ] = pd.to_datetime("2018-02-01")
    dates_travaux.loc[
        dates_travaux.index == "Avenue de l'Europe", "date_fin"
    ] = pd.to_datetime("2019-01-01")
    # pour une fin en déc 18 on met au 1/1/19 à 0h

    dates_travaux.loc[
        dates_travaux.index == "Avenue Victor Hugo", "date_debut"
    ] = pd.to_datetime("2018-01-01")
    dates_travaux.loc[
        dates_travaux.index == "Avenue Victor Hugo", "date_fin"
    ] = pd.to_datetime("2019-06-01")

    dates_travaux.loc[
        dates_travaux.index == "Avenue Gaston Berger", "date_debut"
    ] = pd.to_datetime("2017-11-01")
    dates_travaux.loc[
        dates_travaux.index == "Avenue Gaston Berger", "date_fin"
    ] = pd.to_datetime("2019-01-01")

    dates_travaux.loc[
        dates_travaux.index == "Avenue des Belges", "date_debut"
    ] = pd.to_datetime("2018-01-01")
    dates_travaux.loc[
        dates_travaux.index == "Avenue des Belges", "date_fin"
    ] = pd.to_datetime("2019-06-01")

    dates_travaux.loc[
        dates_travaux.index == "Avenues Schumann -Churchill", "date_debut"
    ] = pd.to_datetime("2017-11-01")
    dates_travaux.loc[
        dates_travaux.index == "Avenues Schumann -Churchill", "date_fin"
    ] = pd.to_datetime("2018-10-01")

    dates_travaux.loc[
        dates_travaux.index == "Avenues Pagnol - Europe", "date_debut"
    ] = pd.to_datetime("2017-11-01")
    dates_travaux.loc[
        dates_travaux.index == "Avenues Pagnol - Europe", "date_fin"
    ] = pd.to_datetime("2019-06-01")

    # -Graph
    for n_lieu in range(len(liste_lieux)):

        # --Courbe des données de comptage
        fig = px.line(
            ts_by_month_lieu_imp,
            x=ts_by_month_lieu_imp.index,
            y=liste_lieux[n_lieu],
            labels={"date": "Mois de comptage", liste_lieux[n_lieu]: "Nombre de vélos"},
            title=liste_lieux[n_lieu]
            + "<br>Nombre moyen de vélos enregistrés par lieu (en 1h)",
        )
        # c'est peut-être un peu lourd de mettre 16 fois "Nombre moyen de vélos enregistrés par lieu (en 1h)", on peut p-ê l'enlever

        # --Courbe de la tendance
        fig.add_scatter(
            x=ts_by_month_lieu_decompose_trend.index,
            y=round(ts_by_month_lieu_decompose_trend.iloc[:, n_lieu]),
            mode="lines",
            name="Tendance",
        )
        fig.update_layout(showlegend=False, font=dict(size=18))

        # --Mention du % de croissance
        fig.add_annotation(
            x=1,
            y=0.8,
            text=msg_perc_croissance(ts_by_month_lieu_imp[liste_lieux[n_lieu]], 0, -1),
            showarrow=False,
            xref="paper",
            xanchor="right",
            yref="paper",
            yanchor="top",
            font=dict(color="#ffffff"),
            bgcolor="red",
            opacity=0.5,
        )

        # --Mention de la période de travaux
        if not pd.isnull(dates_travaux.loc[liste_lieux[n_lieu], "date_debut"]):
            date_deb_travaux = dates_travaux.loc[liste_lieux[n_lieu], "date_debut"]
            date_fin_travaux = dates_travaux.loc[liste_lieux[n_lieu], "date_fin"]
            ytravaux = ts_by_month_lieu_imp[liste_lieux[n_lieu]].max()

            fig.add_annotation(
                x=date_deb_travaux,
                y=ytravaux,
                text="Travaux",
                xanchor="left",
                yanchor="bottom",
                showarrow=False,
                font=dict(color="black", size=14),
                bgcolor="orange",
            )

            fig.add_shape(
                type="line",
                x0=date_deb_travaux,
                y0=ytravaux,
                x1=date_fin_travaux,
                y1=ytravaux,
                line=dict(color="black", width=2),
            )

        # --ylim
        fig.update_yaxes(
            range=[0, ts_by_month_lieu_imp.iloc[:, n_lieu].max() * 1.1], autorange=False
        )

        graphs_data_list.append(fig.to_json())

    print(len(graphs_data_list))
    return graphs_data_list


def make_map():

    flat_data = get_bike_data()

    data_adava = pd.DataFrame(flat_data)

    # ---Import des lieux de comptage
    lieux = pd.read_csv("./lieux_comptage.csv", sep=";")
    lieux["geometry"] = [Point(xy) for xy in zip(lieux["lon"], lieux["lat"])]
    lieux = gpd.GeoDataFrame(lieux)

    # convertir les dates en format date
    data_adava["date"] = pd.to_datetime(data_adava["date"])

    # enlever les données de juillet et août
    juil_aout = (pd.DatetimeIndex(data_adava["date"]).month == 7) | (
        pd.DatetimeIndex(data_adava["date"]).month == 8
    )
    data_adava = data_adava[~juil_aout]

    # enlever les données de mai et juin 2016 où il y a pas mal de points de comptages manquants
    year_month = pd.to_datetime(data_adava["date"]).dt.to_period("M")
    mai_juin_16 = (year_month == "2016-05") | (year_month == "2016-06")
    data_adava = data_adava[~mai_juin_16]

    data_adava.reset_index(drop=True, inplace=True)

    # erreurs sur les dates : mettre toutes les dates avec day=1 et h:m:s.ms = 00:00:00.000
    data_adava["date"] = data_adava["date"].apply(
        lambda t: datetime(t.year, t.month, 1, 0, 0, 0, 0)
    )

    # -Décomposer la série temporelle totale pour extraire la saisonnalité, utile pour imputer les données manquantes

    # série temporelle du nombre de vélo moyen (par lieu) compté par mois
    data_by_month_tot_velos = data_adava.groupby(
        ["date", "lieu"], as_index=False
    ).sum()  # somme sur tous les types de voies et de vélo
    data_by_month_mean = data_by_month_tot_velos.groupby(
        ["date"], as_index=False
    ).mean()
    ts_by_month_mean = data_by_month_mean.set_index("date")

    # décomposition de la série temporelle (period=10 car il n'y a pas de données en juillet-août)
    ts_by_month_mean_decompose = seasonal_decompose(
        ts_by_month_mean, model="additive", period=10, extrapolate_trend=1
    )

    df_lieu_mois = data_adava.groupby(["lieu", "date"])["nombre"].sum()
    df_lieu_mois = pd.DataFrame(df_lieu_mois).reset_index()

    ts_lieux = pd.pivot_table(
        df_lieu_mois, values="nombre", index="date", columns="lieu"
    )

    # imputation des données manquantes
    # choix : calculer la moyenne annuelle (+/- 6 mois) sur ce lieu et ajouter la composante saisonnière calculée plus haut (saisonnalité)
    ts_by_month_lieu_imp = ts_lieux.copy()

    for lieu1 in ts_lieux.columns:
        for date1 in ts_lieux.index:
            if np.isnan(ts_lieux.loc[date1, lieu1]):
                moy_an_lieu = ts_lieux.loc[
                    (date1 - relativedelta(months=7)) : (
                        date1 + relativedelta(months=7)
                    ),
                    lieu1,
                ].mean()
                sais_mois = ts_by_month_mean_decompose.seasonal.loc[date1]
                ts_by_month_lieu_imp.loc[date1, lieu1] = round(
                    moy_an_lieu + sais_mois, 3
                )  # .nombre

    # tendance pour chaque lieu
    ts_by_month_lieu_decompose_trend = pd.DataFrame(
        columns=ts_by_month_lieu_imp.columns
    )
    ts_by_month_lieu_decompose_seasonal = pd.DataFrame(
        columns=ts_by_month_lieu_imp.columns
    )

    for lieu1 in ts_by_month_lieu_imp.columns:
        ts_by_month_lieu_decompose_trend[lieu1] = seasonal_decompose(
            ts_by_month_lieu_imp[lieu1],
            model="additive",
            period=10,
            extrapolate_trend=1,
        ).trend
        ts_by_month_lieu_decompose_seasonal[lieu1] = seasonal_decompose(
            ts_by_month_lieu_imp[lieu1],
            model="additive",
            period=10,
            extrapolate_trend=1,
        ).seasonal

    # ---Carte interactive

    # faire une df avec : date, point, valeur => repivoter
    df_trends_velos = ts_by_month_lieu_decompose_trend.reset_index().melt(
        id_vars="date", var_name="lieu", value_name="nb_velos"
    )
    df_trends_velos = df_trends_velos.merge(lieux)
    df_trends_velos["date_my"] = df_trends_velos["date"].apply(lambda d: str(d)[0:7])

    # changer les noms de variables pour clarifier
    df_trends_velos.rename(
        columns={
            "nb_velos": "Nb vélos",
            "date_my": "Mois",
            "lat": "Latitude",
            "lon": "Longitude",
        },
        inplace=True,
    )

    # arrondir le nombre de vélos pour la légende
    df_trends_velos["Nb vélos"] = df_trends_velos["Nb vélos"].apply(
        lambda nv: round(nv)
    )

    # -Carte
    fig = px.scatter_mapbox(
        df_trends_velos,
        lat="Latitude",
        lon="Longitude",
        size="Nb vélos",
        hover_name="lieu",
        animation_frame="Mois",
        custom_data=["lieu", "Mois", "Nb vélos"],
    ).update_layout(
        mapbox={"style": "carto-positron", "zoom": 12.5},
        margin={"l": 0, "r": 0, "t": 0, "b": 0},
    )

    # étiquette personnalisée
    fig.update_traces(
        hovertemplate="<br>".join(
            [
                "<b>%{customdata[0]}</b> <br>",
                "%{customdata[1]} : %{customdata[2]} vélos <br><i>(moyenne glissante)</i>",
            ]
        )
    )

    # pour accélérer l'avancée du temps
    fig.layout.updatemenus[0].buttons[0].args[1]["frame"]["duration"] = 200

    return fig.to_json()
