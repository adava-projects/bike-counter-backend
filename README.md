```json

{
    "date": "2021-11-01",
    "begin_time": "15:30:00",
    "end_time": "16:30:00",
    "location": "Avenue de l'Europe",
    "data": {
        "Venant de la Rotonde": {
            "chaussee": {
                "velo": 5,
                "vae": 7,
                "trottinettes": 3,
                "autre": 0
            },
            "trottoir": {
                "velo": 4
            },
            "piste_cyclable": {
                "velo": 4
            }
        },
        "Vers de la Rotonde": {
            "chaussee": {
                "velo": 5,
                "vae": 7,
                "trottinettes": 3,
                "autre": 0
            },
            "trottoir": {
                "velo": 4
            },
            "piste_cyclable": {
                "velo": 4
            }
        }
    },
    "meta_data": {
    }
}



```

## Dev

DATABASE_URI=sqlite:///bike_counter/database.db


## TODO

mettre des identifiants et recup le fichier de correspondance

Éditer les données a partir du formulaire
> faire une route qui renvoi les données existantes

vérifier la validation

pour relancer les compteurs : avoir une vue d'ensemble sur ce qui a été fait ou pas
    > simple, lister les données manquantes dans les mois précédents

visualiser les données brutes dans une table PAS NÉCESSAIRE

visu de la carte interactive sur le front aussi

moins urgent: passer les graphs en plotly ok

-> plusieurs pages sur le front



Rendre le % intéractif sur un callback select range dans les plots
